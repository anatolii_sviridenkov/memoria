
// Copyright 2013 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#pragma once

#include <memoria/v1/prototypes/bt/tools/bt_tools.hpp>
#include <memoria/v1/containers/wt/wt_names.hpp>

#include <memoria/v1/core/types/typehash.hpp>

namespace memoria {
namespace v1 {
namespace wt            {
}


template <typename... LabelDescriptors>
struct TypeHash<wt::WTLabeledTree<LabelDescriptors...>>: TypeHash<LabeledTree<LabelDescriptors...>> {};

template <typename... LabelDescriptors>
struct TypeHash<wt::WTLabeledTree<TypeList<LabelDescriptors...>>>: TypeHash<LabeledTree<LabelDescriptors...>> {};





}}