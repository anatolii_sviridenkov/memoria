
// Copyright 2011 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#pragma once

#include <memoria/v1/core/types/types.hpp>
#include <memoria/v1/core/types/typelist.hpp>
#include <memoria/v1/core/container/names.hpp>

#include <memoria/v1/core/container/logs.hpp>

#include <memory>
#include "../tools/pair.hpp"

namespace memoria {
namespace v1 {



template <typename Types> class Ctr;
template <typename Types> class Iter;
template <typename Name, typename Base, typename Types> class IterPart;


template <int Idx, typename Types>
class IterHelper: public IterPart<
                            SelectByIndex<Idx,typename Types::List>,
                            IterHelper<Idx - 1, Types>, Types
                         >
{
    using MyType    = Iter<Types>;
    using ThisType  = IterHelper<Idx, Types>;
    using BaseType  = IterPart<
                SelectByIndex<Idx, typename Types::List>,
                IterHelper<Idx - 1, Types>, Types
    >;

public:
    IterHelper(): BaseType() {}
    IterHelper(ThisType&& other): BaseType(std::move(other)) {}
    IterHelper(const ThisType& other): BaseType(other) {}
};

template <typename Types>
class IterHelper<-1, Types>: public Types::template BaseFactory<Types>::Type {

    typedef Iter<Types>                                                             MyType;
    typedef IterHelper<-1, Types>                                                   ThisType;

    typedef typename Types::template BaseFactory<Types>::Type BaseType;

public:
    IterHelper(): BaseType() {}
    IterHelper(ThisType&& other): BaseType(std::move(other)) {}
    IterHelper(const ThisType& other): BaseType(other) {}
};

template <typename Types>
class IterStart: public IterHelper<ListSize<typename Types::List>::Value - 1, Types> {

    using MyType    = Iter<Types>;
    using ThisType  = IterStart<Types>;
    using Base      = IterHelper<ListSize<typename Types::List>::Value - 1, Types>;
    using ContainerType = Ctr<typename Types::CtrTypes>;

    using CtrPtr    = std::shared_ptr<ContainerType>;

    CtrPtr ctr_ptr_;
    ContainerType* model_;


public:
    IterStart(): Base(), ctr_ptr_(), model_() {}

    IterStart(const CtrPtr& ptr): Base(), ctr_ptr_(ptr), model_(ptr.get()) {}
    IterStart(ThisType&& other): Base(std::move(other)), ctr_ptr_(std::move(other.ctr_ptr_)), model_(other.model_) {}
    IterStart(const ThisType& other): Base(other), ctr_ptr_(other.ctr_ptr_), model_(other.model_) {}

    ContainerType& model() {
        return *model_;
    }

    const ContainerType& model() const {
        return *model_;
    }

    ContainerType& ctr() {
        return *model_;
    }

    const ContainerType& ctr() const {
        return *model_;
    }
};


template <
    typename TypesType
>
class IteratorBase: public TypesType::IteratorInterface {
    typedef IteratorBase<TypesType>                                                 ThisType;

public:

    typedef Ctr<typename TypesType::CtrTypes>                                       Container;
    typedef typename Container::Allocator                                           Allocator;
    typedef typename Allocator::Page::ID                                            PageId;

    typedef Iter<TypesType>                                                         MyType;

    enum {NORMAL = 0, END = 1, START = 2, EMPTY = 3};
    
private:
    Logger logger_;

    Int type_;

    PairPtr pair_;

public:
    IteratorBase():
        logger_("Iterator", Logger::DERIVED, &v1::logger),
        type_(NORMAL)
    {}

    IteratorBase(ThisType&& other): logger_(std::move(other.logger_)), type_(other.type_) {}
    IteratorBase(const ThisType& other): logger_(other.logger_), type_(other.type_)       {}


    PairPtr& pair() {return pair_;}
    const PairPtr& pair() const {return pair_;}


    const Int& type() const {
        return type_;
    }

    Int& type() {
        return type_;
    }

    bool isEqual(const ThisType& other) const
    {
        return true;
    }

    bool isNotEqual(const ThisType& other) const
    {
        return false;
    }

    void assign(const ThisType& other)
    {
        logger_ = other.logger_;
        type_   = other.type_;
    }

    void assign(ThisType&& other)
    {
        logger_ = std::move(other.logger_);
        type_   = other.type_;
    }

    MyType* me() {
        return static_cast<MyType*>(this);
    }

    const MyType* me() const {
        return static_cast<const MyType*>(this);
    }


    bool is_log(Int level)
    {
        return logger_.isLogEnabled(level);
    }

    v1::Logger& logger()
    {
          return logger_;
    }

    const char* typeName() const {
        return me()->model().typeName();
    }
};



//template <typename Container> class IteratorFactoryName {};




}}
