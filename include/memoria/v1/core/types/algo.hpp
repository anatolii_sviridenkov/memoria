
// Copyright 2011 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#pragma once

#include <memoria/v1/core/types/algo/for_each.hpp>
#include <memoria/v1/core/types/algo/minmax.hpp>
#include <memoria/v1/core/types/algo/select.hpp>
#include <memoria/v1/core/types/algo/sort.hpp>
#include <memoria/v1/core/types/algo/indexof.hpp>
#include <memoria/v1/core/types/algo/fold.hpp>

