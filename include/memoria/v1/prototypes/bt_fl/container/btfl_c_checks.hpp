
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#pragma once

#include <memoria/v1/prototypes/bt_fl/btfl_names.hpp>
#include <memoria/v1/core/container/container.hpp>
#include <memoria/v1/core/container/macros.hpp>

#include <memoria/v1/prototypes/bt_fl/btfl_tools.hpp>


#include <vector>

namespace memoria {
namespace v1 {

MEMORIA_V1_CONTAINER_PART_BEGIN(v1::btfl::ChecksName)

public:
    using typename Base::Types;
    using typename Base::IteratorPtr;

protected:
    using typename Base::NodeBaseG;
    using typename Base::NodeDispatcher;
    using typename Base::LeafDispatcher;
    using typename Base::BranchDispatcher;
    using typename Base::Position;
    using typename Base::BranchNodeEntry;
    using typename Base::PageUpdateMgr;
    using typename Base::CtrSizeT;
    using typename Base::CtrSizesT;


    static const Int Streams = Types::Streams;

    using PageUpdateMgt = typename Types::PageUpdateMgr;


    bool checkContent(const NodeBaseG& node) const
    {
    	auto& self = this->self();
    	if (!Base::checkContent(node))
    	{
    		if (node->is_leaf())
    		{
    			auto sizes = self.getLeafStreamSizes(node);

    			CtrSizeT data_streams_size = 0;
    			for (Int c = 0; c < CtrSizesT::Indexes - 1; c++)
    			{
    				data_streams_size += sizes[c];
    			}

    			if (data_streams_size != sizes[Streams - 1])
    			{
    				MEMORIA_ERROR(self, "Leaf streams sizes check failed", data_streams_size, sizes[Streams - 1]);
    				return true;
    			}
    		}

    		return false;
    	}
    	else {
    		return true;
    	}
    }


MEMORIA_V1_CONTAINER_PART_END

#define M_TYPE      MEMORIA_V1_CONTAINER_TYPE(v1::btfl::ChecksName)
#define M_PARAMS    MEMORIA_V1_CONTAINER_TEMPLATE_PARAMS



#undef M_PARAMS
#undef M_TYPE

}}
