
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#pragma once

#include <memoria/v1/core/types/types.hpp>

#include <memoria/v1/core/packed/tools/packed_dispatcher.hpp>
#include <memoria/v1/core/packed/sseq/packed_rle_searchable_seq.hpp>

#include <memoria/v1/prototypes/bt/layouts/bt_input.hpp>
#include <memoria/v1/prototypes/bt_fl/btfl_tools.hpp>

#include <memoria/v1/prototypes/bt_fl/io/btfl_data_input.hpp>
#include <memoria/v1/prototypes/bt_fl/io/btfl_structure_input.hpp>
#include <memoria/v1/prototypes/bt_fl/io/btfl_rank_dictionary.hpp>
#include <memoria/v1/prototypes/bt_fl/io/btfl_flat_tree_structure_generator.hpp>
#include <memoria/v1/prototypes/bt_fl/io/btfl_flat_tree_iobuffer_adapter.hpp>
#include <memoria/v1/prototypes/bt_fl/io/btfl_ctr_input_provider_base.hpp>
#include <memoria/v1/prototypes/bt_fl/io/btfl_iobuffer_ctr_input_provider.hpp>
#include <memoria/v1/prototypes/bt_fl/io/btfl_flat_tree_iobuffer_adapter.hpp>

#include <memory>

namespace memoria {
namespace v1 {
namespace btfl {
namespace io {



}}}}
