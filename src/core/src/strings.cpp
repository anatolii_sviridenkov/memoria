
// Copyright 2011 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.




#include <memoria/v1/core/exceptions/exceptions.hpp>
#include <memoria/v1/core/tools/strings/string.hpp>

#include <errno.h>

namespace memoria {
namespace v1 {

using namespace std;

String trimString(StringRef str)
{
    if (str.length() == 0)
    {
        return "";
    }
    else {
        size_t begin = str.find_first_not_of("\n\r\t ");
        if (begin == String::npos)
        {
            return "";
        }
        else {
            size_t end = str.find_last_not_of("\n\r\t ");
            if (end != String::npos)
            {
                return str.substr(begin, end - begin + 1);
            }
            else {
                return str.substr(begin, str.length() - begin);
            }
        }
    }
}

bool isEndsWith(StringRef str, StringRef end) {
    if (end.length() > str.length())
    {
        return false;
    }
    else {
        UInt l0 = str.length();
        UInt l1 = end.length();
        for (unsigned c = 0; c < end.length(); c++)
        {
            if (str[l0 - l1 + c] != end[c])
            {
                return false;
            }
        }
        return true;
    }
}

bool isStartsWith(StringRef str, StringRef start) {
    if (start.length() > str.length())
    {
        return false;
    }
    else {
        for (unsigned c = 0; c < start.length(); c++)
        {
            if (str[c] != start[c])
            {
                return false;
            }
        }
        return true;
    }
}

bool isEmpty(StringRef str) {
    return str.find_first_not_of("\r\n\t ") == String::npos;
}

bool isEmpty(StringRef str, String::size_type start, String::size_type end, StringRef sep)
{
    if (end == String::npos) end = str.length();

    if (start != String::npos && start < str.length() && start < end - 1)
    {
        String::size_type idx = str.find_first_not_of((sep+"\t ").data(), start);

        if (idx != String::npos)
        {
            return idx >= end;
        }
        else {
            return true;
        }
    }
    else {
        return true;
    }
}

Long strToL(StringRef value) {
    if (!isEmpty(value))
    {
        const char* ptr = trimString(value).c_str();
        char* end_ptr;

        errno = 0;
        Long v = strtol(ptr, &end_ptr, 0);

        if (errno == 0)
        {
            if (*end_ptr == '\0')
            {
                return v;
            }
            else {
                throw Exception(MEMORIA_SOURCE, SBuf()<<"Invalid integer value: "<<value);
            }
        }
        else {
            throw Exception(MEMORIA_SOURCE, SBuf()<<"Invalid integer value: "<<value);
        }
    }
    else {
        throw Exception(MEMORIA_SOURCE, SBuf()<<"Invalid integer value: "<<value);
    }
}

String ReplaceFirst(StringRef str, StringRef txt) {
    return str;
}

String ReplaceLast(StringRef str, StringRef txt) {
    return str;
}

String ReplaceAll(StringRef str, StringRef txt) {
    return str;
}





Int getValueMultiplier(const char* chars, const char* ptr)
{
    if (*ptr == 0) {
        return 1;
    }
    else if (*ptr == 'K') {
        return 1024;
    }
    else if (*ptr == 'M') {
        return 1024*1024;
    }
    else if (*ptr == 'G') {
        return 1024*1024*1024;
    }
    else if (*ptr == 'k') {
        return 1000;
    }
    else if (*ptr == 'm') {
        return 1000*1000;
    }
    else if (*ptr == 'g') {
        return 1000*1000*1000;
    }
    else {
        throw v1::Exception(MEMORIA_SOURCE, SBuf()<<"Invalid number format: "<<chars);
    }
}

void checkError(const char* chars, const char* ptr)
{
    if (*ptr != 0) {
        throw v1::Exception(MEMORIA_SOURCE, SBuf()<<"Invalid number format: "<<chars);
    }
}


long int ConvertToLongInt(StringRef str)
{
    const char* chars = str.c_str();
    char* endptr;

    long int value = strtol(chars, &endptr, 0);

    return value * getValueMultiplier(chars, endptr);
}

unsigned long int ConvertToULongInt(StringRef str)
{
    const char* chars = str.c_str();
    char* endptr;

    unsigned long int value = strtoul(chars, &endptr, 0);

    return value * getValueMultiplier(chars, endptr);
}


long long ConvertToLongLong(StringRef str)
{
    const char* chars = str.c_str();
    char* endptr;

    long long int value = strtoll(chars, &endptr, 0);

    return value * getValueMultiplier(chars, endptr);
}

unsigned long long ConvertToULongLong(StringRef str)
{
    const char* chars = str.c_str();
    char* endptr;

    unsigned long long int value = strtoull(chars, &endptr, 0);

    return value * getValueMultiplier(chars, endptr);
}

double ConvertToDouble(StringRef str)
{
    const char* chars = str.c_str();
    char* endptr;

    double value = strtod(chars, &endptr);

    checkError(chars, endptr);

    return value;
}

long double ConvertToLongDouble(StringRef str)
{
    const char* chars = str.c_str();
    char* endptr;

    long double value = strtod(chars, &endptr);

    checkError(chars, endptr);

    return value;
}

bool ConvertToBool(StringRef str)
{
    if (str == "true" || str == "True" || str == "Yes" || str == "yes" || str == "1")
    {
        return true;
    }
    else if (str == "false" || str == "False" || str == "No" || str == "no" || str == "0")
    {
        return false;
    }
    else {
        throw v1::Exception(MEMORIA_SOURCE, SBuf()<<"Invalid boolean format: "<<str);
    }
}

}}